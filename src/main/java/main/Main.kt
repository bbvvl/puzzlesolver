package main

import puzzles.Sudoku
import java.util.*

object Main {
    @JvmStatic
    fun main(args: Array<String>) {

        val keyboard = Scanner(System.`in`)

        val puzzle = Array(9, { IntArray(9) })

        for (z in 0..8) {
            for (i in 0..8)
                puzzle[z][i] = keyboard.nextInt()
        }
        Sudoku(puzzle).run {
            solvePuzzle()
            printAll()
        }
    }
}