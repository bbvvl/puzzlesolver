package puzzles

import java.util.*
import kotlin.collections.ArrayList

class Sudoku(private var puzzle: Array<IntArray>?) {
    val DEBUG = false

    private val possibleValues: MutableMap<Int, MutableList<Int>> = LinkedHashMap()
    private val resolvedPuzzles = ArrayList<Array<IntArray>>()

    private val minSize: Int
        get() {
            var minSize = 10
            if (resolvedPuzzles.isEmpty())
                for (z in 0..8) {
                    (0..8)
                            .asSequence()
                            .filter { puzzle!![z][it] == 0 && minSize > possibleValues[10 * z + it]!!.size }
                            .forEach { minSize = possibleValues[10 * z + it]!!.size }
                }


            return minSize
        }

    fun solvePuzzle() {

        while (true) {
            if (!basicRulesControl() && !findDuplicates() && !forecast()) break
        }

        when (minSize) {
            10 -> {
                resolvedPuzzles.add(puzzle!!);return
            }
            0 -> return
            else -> partialSearch()
        }
    }

    private fun basicRulesControl(): Boolean {
        if (DEBUG) println("basicRulesControl s")
        var changed = false
        for (z in 0..8)
            (0..8)
                    .filter { puzzle!![z][it] == 0 }
                    .forEach { possibleValues[10 * z + it] = arrayListOf(1, 2, 3, 4, 5, 6, 7, 8, 9) }
        for (z in 0..8)
            (0..8)
                    .filter { puzzle!![z][it] == 0 }
                    .forEach { filterByLinesAndSquares(possibleValues[10 * z + it]!!, z, it); if (checkAndSetValue(z, it)) changed = true }
        if (DEBUG) println("basicRulesControl e")
        return changed
    }

    private fun filterByLinesAndSquares(list: MutableList<Int>, z: Int, i: Int) {

        // vertical line
        (0..8)
                .filter { puzzle!![it][i] != 0 }
                .forEach {
                    list.remove(puzzle!![it][i]) //list.remove(new Integer(puzzle[q][i]));
                }
        //horizontal line
        (0..8)
                .filter { puzzle!![z][it] != 0 }
                .forEach {
                    list.remove(puzzle!![z][it]) // list.remove(new Integer(puzzle[z][q]));
                }
        //square
        for (q in z / 3 * 3 until z / 3 * 3 + 3) {
            (i / 3 * 3 until i / 3 * 3 + 3)
                    .filter { puzzle!![q][it] != 0 }
                    .forEach { list.remove(puzzle!![q][it]) }// list.remove(new Integer(puzzle[q][w]));
        }
    }

    private fun findDuplicates(): Boolean {
        if (DEBUG) println("findDuplicates s")

        var changed = false

        for (z in 0..8)
            (0..8)
                    .filter { puzzle!![z][it] == 0 && findDuplicatesByLinesAndSquare(z, it) }
                    .forEach { changed = true }
        if (DEBUG) println("findDuplicates e ")
        return changed
    }

    private fun findDuplicatesByLinesAndSquare(z: Int, i: Int): Boolean {

        // vertical line
        var numberOfResults = 0
        var numberOfResultsH = 0
        var changed = false

        for (q in 0..8) {
            if (puzzle!![q][i] == 0 && possibleValues[10 * q + i] == possibleValues[10 * z + i])
                numberOfResults++  //list.remove(new Integer(puzzle[q][i]));

            if (puzzle!![z][q] == 0 && possibleValues[10 * z + q] == possibleValues[10 * z + i])
                numberOfResultsH++

        }

        if (numberOfResults == possibleValues[10 * z + i]!!.size) {
            for (q in 0..8)
                if (puzzle!![q][i] == 0 && possibleValues[10 * q + i]!!.size != numberOfResults) {
                    possibleValues[10 * q + i]!!.removeAll(possibleValues[10 * z + i]!!)
                    if (checkAndSetValue(q, i)) changed = true

                }
        }
        //horizont line
        if (numberOfResultsH == possibleValues[10 * z + i]!!.size) {

            for (q in 0..8)
                if (puzzle!![z][q] == 0 && possibleValues[10 * z + q]!!.size != numberOfResultsH) {
                    possibleValues[10 * z + q]!!.removeAll(possibleValues[10 * z + i]!!)
                    if (checkAndSetValue(z, q)) changed = true
                }
        }
        //square
        numberOfResults = 0

        for (q in z / 3 * 3 until z / 3 * 3 + 3) {
            (i / 3 * 3 until i / 3 * 3 + 3)
                    .filter { puzzle!![q][it] == 0 && possibleValues[10 * q + it] == possibleValues[10 * z + i] }
                    .forEach { numberOfResults++ }
        }

        if (numberOfResults == possibleValues[10 * z + i]!!.size) {
            for (q in z / 3 * 3 until z / 3 * 3 + 3) {
                for (w in i / 3 * 3 until i / 3 * 3 + 3)
                    if (puzzle!![q][w] == 0 && possibleValues[10 * q + w]!!.size != numberOfResults) {
                        possibleValues[10 * q + w]!!.removeAll(possibleValues[10 * z + i]!!)
                        if (checkAndSetValue(q, w)) changed = true

                    }
            }
        }
        return changed

    }

    private fun forecast(): Boolean {
        if (DEBUG) println("Forecast s")
        var changed = false
        for (z in 0..8) {
            for (i in 0..8) {
                if (puzzle!![z][i] == 0) {
                    if (findUniqueByLinesAndSquare(z, i)) changed = true
                }
            }
        }
        if (DEBUG) println("Forecast e")
        return changed
    }

    private fun findUniqueByLinesAndSquare(z: Int, i: Int): Boolean {
        l0@ for (e in possibleValues[10 * z + i]!!) {
            // vertical and horizontal line
            for (q in 0..8) {
                if (puzzle!![q][i] == 0 && possibleValues[10 * q + i]!!.contains(e) && z != q) continue@l0
                if (puzzle!![z][q] == 0 && possibleValues[10 * z + q]!!.contains(e) && i != q) continue@l0
            }
            //square
            for (q in z / 3 * 3 until z / 3 * 3 + 3) {
                for (w in i / 3 * 3 until i / 3 * 3 + 3)
                    if (puzzle!![q][w] == 0 && possibleValues[10 * q + w]!!.contains(e) && (z != q || i != w))
                        continue@l0
            }
            setValue(z, i, possibleValues[10 * z + i]!!.indexOf(e))
            return true
        }
        return false
    }

    private fun checkAndSetValue(z: Int, i: Int): Boolean {
        if (possibleValues[10 * z + i]!!.size == 1) {
            setValue(z, i, 0)
            return true
        }
        return false
    }

    private fun partialSearch() {
        if (DEBUG) println("partialSearch s")
        val mS = minSize; if (mS == 0) return
        for (z in 0..8)
            for (i in 0..8) {
                if (puzzle!![z][i] == 0 && mS == possibleValues[10 * z + i]!!.size) {

                    for (valuePos in 0 until possibleValues[10 * z + i]!!.size) {

                        val recursion = Sudoku(clonePuzzle(puzzle)).apply { basicRulesControl(); setValue(z, i, valuePos);solvePuzzle() }
                        if (recursion.minSize == 10) {
                            resolvedPuzzles.addAll(recursion.resolvedPuzzles)
                            return
                        }
                    }
                    return
                }
            }

        if (DEBUG) println("partialSearch e")
    }

    private fun clonePuzzle(qP: Array<IntArray>?): Array<IntArray> {

        val value = Array(9) { IntArray(9) }
        for (z in 0..8)
            for (i in 0..8)
                value[z][i] = qP!![z][i]
        return value
    }

    private fun setValue(z: Int, i: Int, position: Int) {
        puzzle!![z][i] = possibleValues[10 * z + i]!![position]
        if (DEBUG) println("set value to " + z + " " + i + " value = " + puzzle!![z][i])
    }

    private fun print(puzzle: Array<IntArray>?) {
        for (z in 0..8) {
            for (i in 0..8) {
                if (i != 8)
                    print(puzzle!![z][i].toString() + " ")
                else
                    println(puzzle!![z][i])
            }
        }

    }

    fun printAll() {
        for (i in resolvedPuzzles)
            print(i)
    }
}